# NodeJS with MongoDB Helm Chart

## Prerequisites Details

* [Minikube](https://kubernetes.io/docs/tasks/tools/install-minikube/)
* [Helm 3](https://helm.sh/docs/intro/install/)

## Chart Details

This chart implements a combination of a Hello World NodeJS application with mongodb-replicaset chart

## Installing the Chart

To install the chart with the release name `nodejs`:

``` console
git clone https://gitlab.com/burakesme/nodejs-helm3-mongoreplicaset.git nodejs-helm3-mongoreplicaset
helm repo add stable https://kubernetes-charts.storage.googleapis.com/
helm repo add gitlab https://charts.gitlab.io
cd nodejs-helm3-mongoreplicaset/helm-charts/
helm dependency update
helm install nodejs ../nodejs-helm/
```

It is recommended to run production values on a separate namespace

``` console
helm install --create-namespace --values nodejs-helm3-mongoreplicaset/helm-charts/values-production.yaml nodejs nodejs-helm3-mongoreplicaset/helm-charts/ -n prod
```

To check the status of release:
``` console
helm status nodejs
```

To check the status of release on specific namespace (`prod`):

``` console
helm status nodejs -n prod
```

To upgrade the release:

``` console
helm upgrade -f nodejs-helm/values-production.yaml nodejs nodejs-helm3-mongoreplicaset/helm-charts/ -n prod
```


To uninstall the release:

``` console
helm uninstall nodejs
```

## Installing the Chart with Gitlab-runner

``` console
gitlab-runner-registration-token=$(echo -n $gitlab-runner-token | base64) #replace your registration token with $gitlab-runner-token
helm install --set gitlab-runner.enabled=true --set gitlab-runner.runner-registration-token=$gitlab-ruunner-registration-token -f nodejs-helm3-mongoreplicaset/helm-charts/values.yaml  nodejs nodeapp
```
