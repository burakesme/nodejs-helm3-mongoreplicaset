const express = require("express");
const app = express();
const connectDb = require("./src/connection");

const PORT = 8080;

app.get("/", async (req, res) => {
    connectDb().then(() => {
      res.send("Hello World!")
    }).catch((error) => {
       console.log("MongoDB is not ready yet!");
    });
   
});


app.listen(PORT, function() {
  console.log(`Listening on ${PORT}`);

});
