// User.model.js
const mongoose = require("mongoose");
const helloWorldSchema = new mongoose.Schema({
 helloWorld: {
 type: String
 }
});
const HelloWorld = mongoose.model("HelloWorld", helloWorldSchema);
module.exports = HelloWorld;
